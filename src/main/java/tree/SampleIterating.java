package tree;

import com.google.common.collect.TreeTraverser;

/**
 * @author shafin
 * @since 4/11/17
 */
public class SampleIterating {

    public static void main(String[] args) {

        TreeTraverser<TreeNode<String>> traverser = new TreeTraverser<TreeNode<String>>() {
            @Override
            public Iterable<TreeNode<String>> children(TreeNode<String> root) {
                return root.children;
            }
        };

        for (TreeNode<String> node : traverser.preOrderTraversal(SampleData.getSet1())) {
            String indent = createIndent(node.getLevel());
            System.out.println(indent + node.data);
        }
    }

    private static String createIndent(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            sb.append(' ');
        }
        return sb.toString();
    }

}
