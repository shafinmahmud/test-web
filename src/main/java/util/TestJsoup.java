package util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shafin
 * @since 3/7/17
 */
public class TestJsoup {

    public static void main(String[] args) throws IOException {
        //URL link = new URL("https://www.ncdc.noaa.gov/gibbs/availability/1979-01-01");
        String url = "https://www.ncdc.noaa.gov/gibbs/availability/1979-01-01";
        Document doc = Jsoup.connect("https://www.ncdc.noaa.gov/gibbs/availability/1979-01-01").userAgent("Mozilla").get();
        doc.outputSettings().outline(true);

        FileUtil.writeFile("/home/shafin/test.html", getHtml(url));
        //util.FileUtil.writeFile("/home/shafin/test.html", doc.html());

        Elements links = doc.select("#availabilityTable  tr td a");
        System.out.println(links.get(0));

        testInputStream();
    }

    public static String getHtml(String urlTxt) {
        StringBuilder sb = new StringBuilder();

        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            url = new URL(urlTxt);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }

        return sb.toString();
    }

    public static void testInputStream() {
        String line = FileUtil.readFile("/home/shafin/test.txt");
        System.out.println(line);
    }
}
