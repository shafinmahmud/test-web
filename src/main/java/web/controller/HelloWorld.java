package web.controller;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author shafin
 * @since 2/6/17
 */
public class HelloWorld implements Servlet {

    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException {
        this.config = config;
    }

    public void destroy() {
    }

    public ServletConfig getServletConfig() {
        return config;
    }

    public String getServletInfo() {
        return "this is simple hello World Servlet";
    }

    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head>");
        out.println("<title>Simple Servlet</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Hello, World</h1>");
        out.println("</body></html>");
        out.close();
    }
}  