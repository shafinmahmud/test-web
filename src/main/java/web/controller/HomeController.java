package web.controller;

import command.Message;
import domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.EmployeeService;

/**
 * @author shafin
 * @since 3/12/17
 */
@Controller
public class HomeController {

    @Autowired
    private EmployeeService employeeService;

    @ResponseBody
    @GetMapping("/")
    public ResponseEntity<Message> showHome(){
        return new ResponseEntity<Message>(new Message(), HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/employee")
    public ResponseEntity<Employee> getEmployee(@RequestParam("id") long id){
        Employee employee = employeeService.getEmployee(id);

        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }
}
