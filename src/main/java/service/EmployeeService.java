package service;

import domain.Employee;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author shafin
 * @since 3/12/17
 */
@Service
public class EmployeeService {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Employee getEmployee(long id) {
        Employee employee = em.find(Employee.class, id);
        Hibernate.initialize(employee.getPhoneList());
        return employee;
    }
}
