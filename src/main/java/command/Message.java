package command;

/**
 * @author shafin
 * @since 3/12/17
 */
public class Message {

    private String title;
    private String body;

    public Message() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
